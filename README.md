Um das Programm zu starten, muss nur die zugehörige Bat-Datei "startapp"
ausgeführt werden.

Nach dem Start öffnet sich automatisch ein CMD-Fenster, sowie der Browser
mit der Website.

Anfangs begrüßt die Turtle und zeichnet das Logo. Nach Fertigstellung des
Logos kann der Benutzer durch einen Klick auf das Turtle-Fenster die
Hauptseite aufrufen.

Auf dieser Seite wird der Benutzer dazu gebeten zwei Python Files anzugeben.
Nach dem Klick auf "Dateien überprüfen" wird die Ähnlichkeit überprüft
und die Dateien dargestellt.