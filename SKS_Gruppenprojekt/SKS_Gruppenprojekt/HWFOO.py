# HelloWorldFoundOnLineOne
# = HWFOO
# SKS3 - Sebastian Pritz und Severin Pichler

# Kommentare
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import re
import statistics
import chardet

# return whether the position is between two quotes
def isInsideString(string, pos):
    dqts = 0
    sqts = 0
    lastFound = ''
    if pos == 0 or pos == len(string)-1:
        return False
    elif pos > len(string)/2:
        while pos < len(string)-1:
            pos += 1
            if string[pos]=='"':
                dqts += 1
                lastFound = '"'
            elif string[pos]=='\'':
                sqts += 1
                lastFound = '\''
           
    elif pos <= len(string)/2:
        while pos > 0:
            pos -= 1
            if string[pos]=='"':
                dqts += 1
                lastFound = '"'
            elif string[pos]=='\'':
                sqts += 1
                lastFound = '\''


    if lastFound == '':
        return False;
    elif lastFound == '"':
        return dqts%2==1
    elif lastFound == '\'':
        return sqts%2==1

# removes all comments, spaces, tabs, etc.
def preprocess(fileHandle,enc):
    insideBlockComment = False
    blockComment = "'''"
    lines = list()
    
    for line in fileHandle.readlines():
        line = line.decode(enc)
        wasComment = False
        # Strip the line of all strings and interruptive special characters (like \n)
        line = line.lower().strip()
        line = re.sub('\n','',line)

        # If line is inside a block comment, a block end has to be found. Ignore everything else.
        if insideBlockComment:
            blockEndPos = line.find(blockComment)
            # If it has been found, reset comment state and remove commented part.
            if blockEndPos != -1:
                line = line[blockEndPos+len(blockComment):]
                insideBlockComment = False
                expectedBlockEnd = ""
        else:
            # Remove all line comments and code thats commented out.
            commentPos = line.find('#')
            # Also make sure that the # is not inside a string.
            if commentPos != -1 and not isInsideString(line, commentPos):
                line = line[0:commentPos]
                wasComment = True

            # Some programming languages support inline blockcomments between code.
            # Following 2 lines remove all blockcomments that are started AND ended in the same line.
            # Use negative lookahead regex to avoid removing code between blockcomments.
            blockSearch = re.escape(blockComment)+"((?!"+re.escape(blockComment)+").)*"+re.escape(blockComment)+"(?!\")"
            line = re.sub(blockSearch, '', line)
            
            # If there is only a single blockComment inside a line, enter insideBlockComment state and remove the commented part
            blockBeginPos = line.find("'''")
            if blockBeginPos != -1 and not isInsideString(line, blockBeginPos):
                line = line[0:blockBeginPos]
                insideBlockComment = True
                wasComment = True

            # If there is something left in the line, append it to the list
            if not wasComment and len(line)>0:
                lines.append(line)

    return lines

def calc_similarity(lhs, rhs, lineweight):

    # if the normal ratio is less than 100%, do some calculations
    if fuzz.ratio(lhs,rhs) < 100:
        # indepth analysis = stuff in quotes/parentheses gets more weight
        var = indepth_analysis(lhs,rhs)
        ratio = 0

        # if indepth analysis was not possible, calculate similarity using total ratio as well as token similarity and order of elements
        if var[0]==-1:
            ratio += fuzz.ratio(lhs,rhs) * 0.6
            ratio += fuzz.partial_token_sort_ratio(lhs,rhs) * 0.3
            ratio += fuzz.partial_token_set_ratio(lhs,rhs) * 0.1
        else:
            # if it was possible however, weigh it with 50% of the impact and reduce the other impacts severly. --> prioritize stuff in parentheses and quotes
            ratio += var[0] * 0.5
            ratio += fuzz.ratio(var[1],var[2]) * 0.3
            ratio += fuzz.partial_token_sort_ratio(var[1],var[2]) * 0.15
            ratio += fuzz.partial_token_set_ratio(var[1],var[2]) * 0.05
         
        ratio *= min(len(lhs),len(rhs))/max(len(lhs),len(rhs))
    # if its 100% similiar, set it to 100 initially
    else:
        ratio = 100

    # the further the lines are apart, the more percent they lose in identity.
    ratio -= lineweight*0.10*100
    return ratio

def indepth_analysis(str1, str2):
    # get all code between parentheses and quotes
    usRex = re.compile('((\(.*\))|(\".*\"))')
    usMatch1 = re.search(usRex,str1)
    usMatch2 = re.search(usRex,str2)

    if usMatch1 and usMatch2:
        # also return the substrings without the already parsed parenthesis/quote objects
        return [fuzz.ratio(usMatch1.group(0),usMatch2.group(0)),re.sub(usRex,'',str1),re.sub(usRex,'',str2)]
    
    return [-1,None,None]

# return the encoding of the file, to decode it properly
def get_encoding(filename):
    file = open(filename, 'rb')
    rawdata = file.read()
    return chardet.detect(rawdata)['encoding']


def plagiarism_checker(name1, name2):
    referenceFile = open(name1, 'rb')
    candidateFile = open(name2, 'rb')

    enc1 = get_encoding(name1)
    enc2 = get_encoding(name2)

    lineList1 = preprocess(referenceFile, enc1)
    lineList2 = preprocess(candidateFile, enc2)

    maxlines = max(len(lineList1), len(lineList2))

    result = list()
    # referencefile line count
    rlc = 0
    for rline in lineList1:
        # clc = candidatefile line count
        clc = 0
        tmp = -1
        for cline in lineList2:
            linediff = (abs(clc-rlc)/maxlines)
            ratio=calc_similarity(rline,cline,linediff)
            if ratio > tmp:
                tmp=ratio
            #print(rline," and ",cline,': ',ratio)
            clc+=1
        result.append(tmp)
        rlc+=1

    referenceFile.close()
    candidateFile.close()

    return round(statistics.mean(result),2)


# should be: 94%
#print(str(plagiarism_checker("res/coding_example_1.py","res/coding_example_2.py")))

# Should be: 85%
#print(str(plagiarism_checker("res/basics_strings_1.py","res/basics_strings_2.py")))

# Should be: 99%
#print(str(plagiarism_checker("res/1_2_1.py","res/1_2_2.py")))








