"""
Definition of urls for SKS_Gruppenprojekt.
"""

from datetime import datetime
from django.urls import path, include
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('', include('plagiat.urls')),
    path('admin/', admin.site.urls),
    path('plagiat/', include('plagiat.urls')), 
]
