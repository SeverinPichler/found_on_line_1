from django.conf.urls import url
from plagiat import views

urlpatterns = [
    url(r'plagiat/', views.index, name='index'),
    url(r'^$', views.index_logo, name = 'index_logo'),
]