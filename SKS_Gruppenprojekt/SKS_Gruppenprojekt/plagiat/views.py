from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.core.files.storage import FileSystemStorage
import turtle
from turtle import Screen
from HWFOO import plagiarism_checker
import os

def rectangle(l, w):    #create a rectangle
    turtle.begin_fill()
    for i in range(2):
        turtle.right(90)
        turtle.forward(l)
        turtle.right(90)
        turtle.forward(w)
    turtle.end_fill()

def logo():   
    rootwindow = Screen().getcanvas().winfo_toplevel()
    rootwindow.call('wm', 'attributes', '.', '-topmost', '1')
    rootwindow.call('wm', 'attributes', '.', '-topmost', '0')

    #set up turtle
    turtle.hideturtle()
    turtle.color('#F7B10C')
    turtle.pensize(2)
    turtle.penup()

    #draw big blue rectangle
    turtle.setposition(350, 150)
    turtle.pendown()
    rectangle(90,700)
    turtle.penup()

    #draw smaller red rectangle
    turtle.color('#FF2F00')
    turtle.setposition(-10,135)
    turtle.pendown()
    rectangle(50,250)
    turtle.penup()

    #write the text
    turtle.setposition(0,90)
    turtle.color('black')
    style = ('comic sans', 30, 'italic')
    turtle.write('1     Hello world        1     Hello world', font=style, align='center')


    #draw arrow
    turtle.setposition(-200,1)
    turtle.pendown()
    turtle.showturtle()
    turtle.forward(100)
    turtle.stamp()
    turtle.hideturtle()
    turtle.penup()

    #write 100%
    turtle.setposition(0,-25)
    style = ('comic sans', 30, 'italic')
    turtle.write('100 %',  font=style, align='center')
    turtle.exitonclick()
 
def read_save_file(filename):
    lines = []
    with open(filename, "r") as fi:
        for line in fi:
            lines.append(str(line))
        
    return lines

def index_logo(request):
    logo()  #create logo
    return HttpResponseRedirect('http://127.0.0.1:8080/plagiat')   #switch to /plagiat 


# Create your views here.
def index(request):

    #save the 2 files in the project folder
    if request.method == 'POST':
        context = {}
        
        #check if files got selected and if the files contain .py
        if len(request.FILES.getlist('f1')) != 0 and len(request.FILES.getlist('f2')) != 0:
            if request.FILES['f1'].name.lower().endswith('.py') and request.FILES['f1'].name.lower().endswith('.py'):
            
                myfile = request.FILES['f1']    #get names of files
                myfile2 = request.FILES['f2']
                
                fs = FileSystemStorage()    #save files in folder
                filename = fs.save(myfile.name, myfile)
                filename2 = fs.save(myfile2.name, myfile2)
             
                result = plagiarism_checker(filename, filename2)    #calculate similarity

                f1_cont = read_save_file(filename)
                f2_cont = read_save_file(filename2)
                
                context = {'result': result,
                            'f1_cont': f1_cont,
                            'f2_cont': f2_cont,
                            'filename': filename,
                            'filename2': filename2}
                            
                os.remove(filename)
                os.remove(filename2)
                
                return render(request, 'plagiat/index_result.html', context)
            
        return render(request, 'plagiat/index_wrong.html')
        
    return render(request, 'plagiat/index.html')
    
    
    